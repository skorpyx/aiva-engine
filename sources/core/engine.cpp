#include <core/engine.h>
#include <utils/config.h>
#include <utils/debug.h>

void Engine::Loop()
{
    if (isQuitRequested)
    {
        Debug::Crash(u8"Just let this engine die\n");
        return;
    }

    Debug::Log(u8"Init()\n");

    while (!isQuitRequested)
    {
        hardwareModule.Tick();
    }

    Debug::Log(u8"Shutdown()\n");
}

// EngineForModule ------------------------------------------------------------

void Engine::Quit()
{
    isQuitRequested = true;
}
