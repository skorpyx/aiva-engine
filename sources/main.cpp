#include <cstdlib>
#include <core/engine.h>

int main(int, char**)
{
    Engine{}.Loop();
    return EXIT_SUCCESS;
}
