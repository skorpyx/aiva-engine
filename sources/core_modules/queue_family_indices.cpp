#include <core_modules/queue_family_indices.h>

bool QueueFamilyIndices::IsSet() const
{
    return Graphics.IsSet() && Present.IsSet();
}
