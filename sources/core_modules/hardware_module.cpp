#include <core_modules/hardware_module.h>
#include <utils/config.h>
#include <utils/debug.h>

HardwareModule::HardwareModule(EngineForModule& engine)
:
    Engine { engine },
    Window { nullptr },
    Instance { VK_NULL_HANDLE },
    Surface { VK_NULL_HANDLE },
    PhysicalDevice { VK_NULL_HANDLE },
    LogicalDevice { VK_NULL_HANDLE }
{
    InitSDL();
    InitSDLWindow();
    InitVulkanInstance();
    InitVulkanSurface();
    InitVulkanPhysicalDevice();
    InitVulkanLogicalDevice();
}

HardwareModule::~HardwareModule()
{
    ShutdownVulkanLogicalDevice();
    ShutdownVulkanPhysicalDevice();
    ShutdownVulkanSurface();
    ShutdownVulkanInstance();
    ShutdownSDLWindow();
    ShutdownSDL();
}

void HardwareModule::Tick()
{
    auto event = SDL_Event {};

    while (SDL_PollEvent(&event))
    {
        if (event.type == SDL_EventType::SDL_QUIT)
        {
            Engine.Quit();
            return;
        }
    }
}

void HardwareModule::InitSDL()
{
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        Debug::Crash(u8"Failed to initialize SDL: %s\n", SDL_GetError());
        return;
    }
}

void HardwareModule::InitSDLWindow()
{
    if (auto const window = SDL_CreateWindow
    (
        Config::ProjectName(),
        SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED,
        Config::ScreenWidth(),
        Config::ScreenHeight(),
        SDL_WINDOW_VULKAN
    ))
    {
        Window = window;
    }
    else
    {
        Debug::Crash(u8"Failed to initialize SDL window: %s\n", SDL_GetError());
        return;
    }
}

void HardwareModule::InitVulkanInstance()
{
    auto appInfo = VkApplicationInfo {};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = Config::ProjectName();
    appInfo.applicationVersion = VK_MAKE_API_VERSION(0, 1, 0, 0);
    appInfo.pEngineName = Config::EngineName();
    appInfo.engineVersion = VK_MAKE_API_VERSION(0, 1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_0;

    auto createInfo = VkInstanceCreateInfo {};
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = &appInfo;

    auto const extensions = VulkanGetInstanceExtensions(Window);
    createInfo.enabledExtensionCount = extensions.Size();
    createInfo.ppEnabledExtensionNames = *extensions;

    if (vkCreateInstance(&createInfo, nullptr, &Instance) != VK_SUCCESS)
    {
        Debug::Crash(u8"Failed to initialize Vulkan instance\n");
        return;
    }
}

void HardwareModule::InitVulkanSurface()
{
    if (!SDL_Vulkan_CreateSurface(Window, Instance, &Surface))
    {
        Debug::Crash(u8"Failed to initialize SDL surface: %s\n", SDL_GetError());
        return;
    }
}

void HardwareModule::InitVulkanPhysicalDevice()
{
    auto const device =
        VulkanGetPhysicalDevices(Instance)
        .FirstOrDefault([this](VkPhysicalDevice const& physicalDevice)
        {
            return VulkanIsSuitable(Surface, physicalDevice);
        });

    if (device != VK_NULL_HANDLE)
    {
        PhysicalDevice = device;
    }
    else
    {
        Debug::Crash(u8"Failed to find any suitable Vulkan GPU\n");
        return;
    }
}

void HardwareModule::InitVulkanLogicalDevice()
{
    auto const queueIndices = VulkanGetQueueFamilyIndices(Surface, PhysicalDevice);
    auto const queuePriority = 1.0f;
    auto deviceFeatures = VkPhysicalDeviceFeatures {};

    auto queueCreateInfo = VkDeviceQueueCreateInfo {};
    queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queueCreateInfo.queueFamilyIndex = queueIndices.Graphics.Get();
    queueCreateInfo.queueCount = 1;
    queueCreateInfo.pQueuePriorities = &queuePriority;

    auto deviceCreateInfo = VkDeviceCreateInfo {};
    deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    deviceCreateInfo.pQueueCreateInfos = &queueCreateInfo;
    deviceCreateInfo.queueCreateInfoCount = 1;
    deviceCreateInfo.pEnabledFeatures = &deviceFeatures;

    if (vkCreateDevice(PhysicalDevice, &deviceCreateInfo, nullptr, &LogicalDevice) != VK_SUCCESS)
    {
        Debug::Crash(u8"Failed to initialize Vulkan logical device\n");
        return;
    }
}

void HardwareModule::ShutdownVulkanLogicalDevice()
{
    if (LogicalDevice != VK_NULL_HANDLE)
    {
        vkDestroyDevice(LogicalDevice, nullptr);
        LogicalDevice = VK_NULL_HANDLE;
    }
    else
    {
        Debug::Crash(u8"Failed to destroy Vulkan logical device\n");
        return;
    }
}

void HardwareModule::ShutdownVulkanPhysicalDevice()
{
    if (PhysicalDevice != VK_NULL_HANDLE)
    {
        PhysicalDevice = VK_NULL_HANDLE;
    }
    else
    {
        Debug::Crash(u8"Failed to destroy Vulkan physical device\n");
        return;
    }
}

void HardwareModule::ShutdownVulkanSurface()
{
    if (Instance != VK_NULL_HANDLE && Surface != VK_NULL_HANDLE)
    {
        vkDestroySurfaceKHR(Instance, Surface, nullptr);
    }
    else
    {
        Debug::Crash(u8"Failed to destroy Vulkan surface\n");
        return;
    }
}

void HardwareModule::ShutdownVulkanInstance()
{
    if (Instance != VK_NULL_HANDLE)
    {
        vkDestroyInstance(Instance, nullptr);
        Instance = VK_NULL_HANDLE;
    }
    else
    {
        Debug::Crash(u8"Failed to destroy Vulkan instance\n");
        return;
    }
}

void HardwareModule::ShutdownSDLWindow()
{
    if (Window != nullptr)
    {
        SDL_DestroyWindow(Window);
        Window = nullptr;
    }
    else
    {
        Debug::Crash(u8"Failed to destroy SDL window\n");
        return;
    }
}

void HardwareModule::ShutdownSDL()
{
    SDL_Quit();
}

Array<char const*> HardwareModule::VulkanGetInstanceExtensions(SDL_Window *const window)
{
    auto extensionsCount = uint32 {};
    if (!SDL_Vulkan_GetInstanceExtensions(window, &extensionsCount, nullptr))
    {
        Debug::Crash(u8"Failed to get Vulkan instance extensions count: %s\n", SDL_GetError());
        return {};
    }

    if (extensionsCount == 0)
    {
        Debug::Crash(u8"Failed to find any Vulkan instance extension\n");
        return {};
    }

    auto extensionsList = Array<char const*> {};
    extensionsList.SetSizeUnitialized(extensionsCount);

    if (!SDL_Vulkan_GetInstanceExtensions(window, &extensionsCount, *extensionsList))
    {
        Debug::Crash(u8"Failed to get Vulkan instance extensions list: %s\n", SDL_GetError());
        return {};
    }

    return extensionsList;
}

Array<VkPhysicalDevice> HardwareModule::VulkanGetPhysicalDevices(VkInstance const instance)
{
    auto devicesCount = uint32 {};
    if (vkEnumeratePhysicalDevices(instance, &devicesCount, nullptr) != VK_SUCCESS)
    {
        Debug::Crash(u8"Failed to get Vulkan physical devices count\n");
        return {};
    }

    if (devicesCount == 0)
    {
        Debug::Crash(u8"Failed to find any GPUs with Vulkan support\n");
        return {};
    }

    auto devicesList = Array<VkPhysicalDevice> {};
    devicesList.SetSizeUnitialized(devicesCount);

    if (vkEnumeratePhysicalDevices(instance, &devicesCount, *devicesList) != VK_SUCCESS)
    {
        Debug::Crash(u8"Failed to get Vulkan physical devices list\n");
        return {};
    }

    return devicesList;
}

Array<VkQueueFamilyProperties> HardwareModule::VulkanGetQueueFamilyProperties(VkPhysicalDevice const physicalDevice)
{
    auto propertiesCount = uint32 {};
    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &propertiesCount, nullptr);

    if (propertiesCount == 0)
    {
        Debug::Crash(u8"Failed to find any Vulkan queue family property\n");
        return {};
    }

    auto propertiesList = Array<VkQueueFamilyProperties> {};
    propertiesList.SetSizeUnitialized(propertiesCount);

    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &propertiesCount, *propertiesList);
    return propertiesList;
}

bool HardwareModule::VulkanGetSurfaceSupport(VkSurfaceKHR const surface, VkPhysicalDevice const physicalDevice, uint32 const queueFamilyIndex)
{
    auto supports = VkBool32 {};
    vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, queueFamilyIndex, surface, &supports);
    return supports;
}

QueueFamilyIndices HardwareModule::VulkanGetQueueFamilyIndices(VkSurfaceKHR const surface, VkPhysicalDevice const physicalDevice)
{
    auto const properties = VulkanGetQueueFamilyProperties(physicalDevice);
    auto indices = QueueFamilyIndices {};

    for (auto i = SizeType {}; i < properties.Size(); i++)
    {
        if (properties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)
        {
            indices.Graphics = i;
            break;
        }
    }

    for (auto i = SizeType {}; i < properties.Size(); i++)
    {
        if (VulkanGetSurfaceSupport(surface, physicalDevice, i))
        {
            indices.Present = i;
            break;
        }
    }

    return indices;
}

VkPhysicalDeviceProperties HardwareModule::VulkanGetProperties(VkPhysicalDevice const physicalDevice)
{
    auto properties = VkPhysicalDeviceProperties {};
    vkGetPhysicalDeviceProperties(physicalDevice, &properties);
    return properties;
}

bool HardwareModule::VulkanIsSuitable(VkSurfaceKHR const surface, VkPhysicalDevice const physicalDevice)
{
    auto properties = VulkanGetProperties(physicalDevice);
    if (properties.deviceType != VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
    {
        return false;
    }

    auto queueFamilyIndices = VulkanGetQueueFamilyIndices(surface, physicalDevice);
    if (!queueFamilyIndices.IsSet())
    {
        return false;
    }

    return true;
}
