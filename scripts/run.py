import sys
import subprocess

AIVA_CONFIG = sys.argv[1] if len(sys.argv) >= 2 else "debug"

subprocess.call("premake5 gmake2")
subprocess.call("mingw32-make config={}".format(AIVA_CONFIG))
subprocess.call("../binaries/{}/aiva".format(AIVA_CONFIG))
