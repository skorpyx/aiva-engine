workspace "aiva"
    configurations { "debug", "release" }

project "aiva"
    kind "ConsoleApp"
    language "C++"
    cppdialect "C++17"
    exceptionhandling "Off"
    flags { "FatalWarnings", "ShadowedVariables" }
    warnings "Extra"
    
    links { "mingw32", "SDL2main", "SDL2", "vulkan-1" }
    
    includedirs { "../headers" }
    files { "../headers/**.h", "../sources/**.cpp" }

    filter "configurations:debug"
        defines { "DEBUG" }
        symbols "On"
        objdir "../objects/debug"
        targetdir "../binaries/debug"

    filter "configurations:release"
        defines { "NDEBUG" }
        optimize "On"
        objdir "../objects/release"
        targetdir "../binaries/release"
