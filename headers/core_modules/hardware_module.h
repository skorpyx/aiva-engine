#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>
#include <vulkan/vulkan.h>
#include <core_interfaces/engine_for_module.h>
#include <core_modules/queue_family_indices.h>
#include <utils/array.h>
#include <utils/non_copyable.h>

class HardwareModule final : public NonCopyable
{
public:
    HardwareModule(EngineForModule& engine);
    ~HardwareModule();

    void Tick();

private:
    EngineForModule& Engine;

    // ------------------------------------------------------------------------
    // INITIALIZATION
    // ------------------------------------------------------------------------

    void InitSDL();
    void InitSDLWindow();
    void InitVulkanInstance();
    void InitVulkanSurface();
    void InitVulkanPhysicalDevice();
    void InitVulkanLogicalDevice();
    void ShutdownVulkanLogicalDevice();
    void ShutdownVulkanPhysicalDevice();
    void ShutdownVulkanSurface();
    void ShutdownVulkanInstance();
    void ShutdownSDLWindow();
    void ShutdownSDL();

    // ------------------------------------------------------------------------
    // UTILS
    // ------------------------------------------------------------------------

    static Array<char const*> VulkanGetInstanceExtensions(SDL_Window *const window);
    static Array<VkPhysicalDevice> VulkanGetPhysicalDevices(VkInstance const instance);
    static Array<VkQueueFamilyProperties> VulkanGetQueueFamilyProperties(VkPhysicalDevice const physicalDevice);
    static bool VulkanGetSurfaceSupport(VkSurfaceKHR const surface, VkPhysicalDevice const physicalDevice, uint32 const queueFamilyIndex);
    static QueueFamilyIndices VulkanGetQueueFamilyIndices(VkSurfaceKHR const surface, VkPhysicalDevice const physicalDevice);
    static VkPhysicalDeviceProperties VulkanGetProperties(VkPhysicalDevice const physicalDevice);
    static bool VulkanIsSuitable(VkSurfaceKHR const surface, VkPhysicalDevice const physicalDevice);

    // ------------------------------------------------------------------------
    // DATA
    // ------------------------------------------------------------------------

    SDL_Window* Window;
    VkInstance Instance;
    VkSurfaceKHR Surface;
    VkPhysicalDevice PhysicalDevice;
    VkDevice LogicalDevice;
};
