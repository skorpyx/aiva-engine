#pragma once

#include <utils/numeric.h>
#include <utils/optional.h>

struct QueueFamilyIndices
{
    Optional<uint32> Graphics {};
    Optional<uint32> Present {};

    bool IsSet() const;
};
