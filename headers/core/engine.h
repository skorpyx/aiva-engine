#pragma once

#include <core_interfaces/engine_for_module.h>
#include <core_modules/hardware_module.h>
#include <utils/non_copyable.h>

class Engine final : public NonCopyable, public EngineForModule
{
public:
    void Loop();

    // EngineForModule --------------------------------------------------------
    void Quit() override;

private:
    // Modules ----------------------------------------------------------------
    HardwareModule hardwareModule { *this };

    // EngineForModule --------------------------------------------------------
    bool isQuitRequested { false };
};
