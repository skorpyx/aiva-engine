#pragma once

class EngineForModule
{
public:
    virtual void Quit() = 0;
    virtual ~EngineForModule() = default;
};
