#pragma once

#include <utils/remove_reference.h>

template <typename T>
T&& Forward(typename RemoveReference<T>::Type& obj)
{
    return static_cast<T&&>(obj);
}

template <typename T>
T&& Forward(typename RemoveReference<T>::Type&& obj)
{
    return static_cast<T&&>(obj);
}
