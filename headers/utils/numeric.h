#pragma once

#include <cstdint>
#include <cfloat>

using int8 = int8_t;
using int16 = int16_t;
using int32 = int32_t;
using int64 = int64_t;

using uint8 = uint8_t;
using uint16 = uint16_t;
using uint32 = uint32_t;
using uint64 = uint64_t;

using float32 = float;
using float64 = double;

using SizeType = size_t;

// ----------------------------------------------------------------------------

class Numeric final
{
public:
    template <typename TNumber>
    static constexpr TNumber Min();

    template <typename TNumber>
    static constexpr TNumber Max();
};

// ----------------------------------------------------------------------------

template <> constexpr int8 Numeric::Min<int8>() { return INT8_MIN; }
template <> constexpr int16 Numeric::Min<int16>() { return INT16_MIN; }
template <> constexpr int32 Numeric::Min<int32>() { return INT32_MIN; }
template <> constexpr int64 Numeric::Min<int64>() { return INT64_MIN; }
template <> constexpr uint8 Numeric::Min<uint8>() { return 0; }
template <> constexpr uint16 Numeric::Min<uint16>() { return 0; }
template <> constexpr uint32 Numeric::Min<uint32>() { return 0; }
template <> constexpr uint64 Numeric::Min<uint64>() { return 0; }
template <> constexpr float32 Numeric::Min<float32>() { return FLT_MIN; }
template <> constexpr float64 Numeric::Min<float64>() { return DBL_MIN; }

template <> constexpr int8 Numeric::Max<int8>() { return INT8_MAX; }
template <> constexpr int16 Numeric::Max<int16>() { return INT16_MAX; }
template <> constexpr int32 Numeric::Max<int32>() { return INT32_MAX; }
template <> constexpr int64 Numeric::Max<int64>() { return INT64_MAX; }
template <> constexpr uint8 Numeric::Max<uint8>() { return UINT8_MAX; }
template <> constexpr uint16 Numeric::Max<uint16>() { return UINT16_MAX; }
template <> constexpr uint32 Numeric::Max<uint32>() { return UINT32_MAX; }
template <> constexpr uint64 Numeric::Max<uint64>() { return UINT64_MAX; }
template <> constexpr float32 Numeric::Max<float32>() { return FLT_MAX; }
template <> constexpr float64 Numeric::Max<float64>() { return DBL_MAX; }
