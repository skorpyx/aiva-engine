#pragma once

#include <utils/debug.h>
#include <utils/math.h>
#include <utils/memory.h>
#include <utils/non_copyable.h>

template <typename TElement>
class Array final
{
public:
    Array() = default;
    ~Array();

    Array(const Array& other);
    Array(Array&& other);
    Array& operator=(const Array& other);
    Array& operator=(Array&& other);

    TElement& operator[](SizeType const index);
    TElement const& operator[](SizeType const index) const;
    TElement* operator*() const;

    SizeType Size() const;

    Array& Clear();
    Array& RemoveAt(SizeType const index, bool const allowShrinking = true);
    Array& SetSizeUnitialized(SizeType const newSize);

    // ----------------------------------------------------

    template <typename TPredicate>
    TElement FirstOrDefault(TPredicate const& predicate) const;

private:
    Array& Shrink();

    static constexpr SizeType const growthFactor = 2;

    TElement* data {};
    SizeType capacity {};
    SizeType size {};
};

// ----------------------------------------------------------------------------

template <typename TElement>
Array<TElement>::~Array()
{
    Clear();
}

template <typename TElement>
Array<TElement>::Array(const Array& other)
{
    if (other.data)
    {
        data = Memory::Memcpy<TElement>(other.data, Memory::Malloc<TElement>(other.capacity), other.size);
        capacity = other.capacity;
        size = other.size;
    }
}

template <typename TElement>
Array<TElement>::Array(Array&& other)
{
    data = other.data;
    other.data = {};

    capacity = other.capacity;
    other.capacity = {};

    size = other.size;
    other.size = {};
}

template <typename TElement>
Array<TElement>& Array<TElement>::operator=(const Array& other)
{
    Clear();

    if (other.data)
    {
        data = Memory::Memcpy<TElement>(other.data, Memory::Malloc<TElement>(other.capacity), other.size);
        capacity = other.capacity;
        size = other.size;
    }

    return *this;
}

template <typename TElement>
Array<TElement>& Array<TElement>::operator=(Array&& other)
{
    Clear();

    data = other.data;
    other.data = {};

    capacity = other.capacity;
    other.capacity = {};

    size = other.size;
    other.size = {};

    return *this;
}

template <typename TElement>
TElement& Array<TElement>::operator[](SizeType const index)
{
    if (index < SizeType {} || index >= size)
    {
        Debug::Crash(u8"Index (%zu) is out of bounds\n", index);

        static TElement defaultElement {};
        return defaultElement;
    }

    return data[index];
}

template <typename TElement>
TElement const& Array<TElement>::operator[](SizeType const index) const
{
    if (index < SizeType {} || index >= size)
    {
        Debug::Crash(u8"Index (%zu) is out of bounds\n", index);

        static TElement const defaultElement {};
        return defaultElement;
    }

    return data[index];
}

template <typename TElement>
TElement* Array<TElement>::operator*() const
{
    if (data)
    {
        return data;
    }
    else
    {
        Debug::Crash(u8"Data is not valid\n");
        return nullptr;
    }
}

template <typename TElement>
SizeType Array<TElement>::Size() const
{
    return size;
}

template <typename TElement>
Array<TElement>& Array<TElement>::Clear()
{
    while (size)
    {
        RemoveAt(size - 1, false);
    }

    return *this;
}

template <typename TElement>
Array<TElement>& Array<TElement>::RemoveAt(SizeType const index, bool const allowShrinking /*= true*/)
{
    // check ----------------------------------------------
    if (index < SizeType {} || index >= size)
    {
        Debug::Crash(u8"Index (%zu) is out of bounds\n", index);
        return *this;
    }

    // remove element -------------------------------------
    (data + index)->~TElement();

    if (index != size - 1)
    {
        auto const tailPos = data + index + 1;
        auto const tailCount = size - index - 1;
        Memory::Memmove(tailPos, tailPos - 1, tailCount);
    }

    size--;

    // resize array ---------------------------------------
    if (size == SizeType {})
    {
        data = Memory::Free<TElement>(data);
        capacity = {};
    }
    else if (allowShrinking)
    {
        Shrink();
    }

    return *this;
}

template <typename TElement>
Array<TElement>& Array<TElement>::SetSizeUnitialized(SizeType const newSize)
{
    while (newSize < size)
    {
        RemoveAt(size - 1, false);
    }
    size = newSize;
    Shrink();
    return *this;
}

template <typename TElement>
template <typename TPredicate>
TElement Array<TElement>::FirstOrDefault(TPredicate const& predicate) const
{
    for (auto i = SizeType {}; i < size; i++)
    {
        auto const& element = data[i];
        if (predicate(element))
        {
            return element;
        }
    }

    return {};
}

template <typename TElement>
Array<TElement>& Array<TElement>::Shrink()
{
    if (capacity == size)
    {
        return *this;
    }

    auto const newCapacity = Math::NextPowerOf(size, growthFactor);
    if (newCapacity == capacity)
    {
        return *this;
    }

    data = Memory::Realloc(data, newCapacity);
    capacity = newCapacity;
    return *this;
}
