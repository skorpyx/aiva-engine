#pragma once

#include <utils/debug.h>
#include <utils/move.h>

template <typename TElement>
class Optional final
{
public:
    Optional();
    Optional(TElement&& value);
    Optional(const TElement& value);

    TElement& Get();
    TElement const& Get() const;
    bool IsSet() const;

private:
    TElement Value {};
    bool HasValue {};
};

// ----------------------------------------------------------------------------

template <typename TElement>
Optional<TElement>::Optional()
{
    Value = {};
    HasValue = false;
}

template <typename TElement>
Optional<TElement>::Optional(TElement&& value)
{
    Value = Move(value);
    HasValue = true;
}

template <typename TElement>
Optional<TElement>::Optional(const TElement& value)
{
    Value = value;
    HasValue = true;
}

template <typename TElement>
TElement& Optional<TElement>::Get()
{
    if (!HasValue)
    {
        Debug::Crash(u8"Optional has no value\n");
    }

    return Value;
}

template <typename TElement>
TElement const& Optional<TElement>::Get() const
{
    if (!HasValue)
    {
        Debug::Crash(u8"Optional has no value\n");
    }

    return Value;
}

template <typename TElement>
bool Optional<TElement>::IsSet() const
{
    return HasValue;
}
