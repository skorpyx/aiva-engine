#pragma once

#include <utils/remove_reference.h>

template <typename T>
typename RemoveReference<T>::Type&& Move(T&& value)
{
    return static_cast<typename RemoveReference<T>::Type&&>(value);
}
