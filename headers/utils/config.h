#pragma once

#include <utils/numeric.h>

class Config final
{
public:
    static auto EngineName() { return engineName; }
    static auto ProjectName() { return projectName; }
    static auto ScreenWidth() { return screenWidth; }
    static auto ScreenHeight() { return screenHeight; }

private:
    Config() = delete;

    static constexpr char const*const engineName { u8"Aiva Engine" };
    static constexpr char const*const projectName { u8"Underground" };
    static constexpr int32 const screenWidth { 1280 };
    static constexpr int32 const screenHeight { 720 };
};
