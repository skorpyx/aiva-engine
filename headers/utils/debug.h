#pragma once

#include <cstdlib>
#include <cstdio>
#include <utils/config.h>
#include <utils/forward.h>

class Debug final
{
public:
    template <typename... TArgs>
    static void Log(char const*const message, TArgs&&... args);

    template <typename... TArgs>
    static void Crash(char const*const message, TArgs&&... args);

private:
    Debug() = delete;
};

// ----------------------------------------------------------------------------

template <typename... TArgs>
void Debug::Log(char const*const message, TArgs&&... args)
{
    printf("%s : ", Config::ProjectName());
    printf(message, Forward<TArgs>(args)...);
}

template <typename... TArgs>
void Debug::Crash(char const*const message, TArgs&&... args)
{
    printf("%s : ", Config::ProjectName());
    printf(message, Forward<TArgs>(args)...);
    exit(EXIT_FAILURE);
}
