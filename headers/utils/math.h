#pragma once

#include <utils/debug.h>
#include <utils/numeric.h>

class Math final
{
public:
    template <typename TNumber>
    static bool CheckMulOverflow(TNumber const a, TNumber const b);

    template <typename TNumber>
    static TNumber NextPowerOf(TNumber const number, TNumber const base);

private:
    Math() = delete;
};

// ----------------------------------------------------------------------------

template <typename TNumber>
bool Math::CheckMulOverflow(TNumber const a, TNumber const b)
{
    if (a < 0 || b < 0)
    {
        Debug::Crash(u8"Unimplemented for negative numbers\n");
        return {};
    }

    return b != 0 && a > Numeric::Max<TNumber>() / b;
}

template <typename TNumber>
TNumber Math::NextPowerOf(TNumber const number, TNumber const base)
{
    if (number < 0)
    {
        Debug::Crash(u8"Number should be greater or equal than 0\n");
        return {};
    }

    if (base <= 1)
    {
        Debug::Crash(u8"Base should be greater than 1\n");
        return {};
    }

    auto power = TNumber { 1 };
    while (true)
    {
        if (power > number)
        {
            break;
        }

        if (CheckMulOverflow(power, base))
        {
            Debug::Crash(u8"Multiplication overflow\n");
            return {};
        }

        power *= base;
    }
    return power;
}
