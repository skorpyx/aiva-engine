#pragma once

#include <cstdlib>
#include <cstring>
#include <utils/debug.h>
#include <utils/numeric.h>

class Memory final
{
public:
    template <typename TElement>
    static TElement* Malloc(SizeType const count);

    template <typename TElement>
    static TElement* Memcpy(TElement const*const from, TElement *const to, SizeType const count);

    template <typename TElement>
    static TElement* Memmove(TElement const*const from, TElement *const to, SizeType const count);

    template <typename TElement>
    static TElement* Realloc(TElement *const memory,  SizeType const count);

    template <typename TElement>
    static TElement* Free(TElement *const memory);

private:
    Memory() = delete;
};

// ----------------------------------------------------------------------------

template <typename TElement>
TElement* Memory::Malloc(SizeType const count)
{
    return static_cast<TElement*>(malloc(count * sizeof(TElement)));
}

template <typename TElement>
TElement* Memory::Memcpy(TElement const*const from, TElement *const to, SizeType const count)
{
    return static_cast<TElement*>(memcpy(to, from, count * sizeof(TElement)));
}

template <typename TElement>
TElement* Memory::Memmove(TElement const*const from, TElement *const to, SizeType const count)
{
    return static_cast<TElement*>(memmove(to, from, count * sizeof(TElement)));
}

template <typename TElement>
TElement* Memory::Realloc(TElement *const memory, SizeType const count)
{
    auto const newMemory = static_cast<TElement*>(realloc(memory, count * sizeof(TElement)));
    if (!newMemory)
    {
        Debug::Crash(u8"Out of memory\n");
    }

    return newMemory;
}

template <typename TElement>
TElement* Memory::Free(TElement *const memory)
{
    free(memory);
    return nullptr;
}
